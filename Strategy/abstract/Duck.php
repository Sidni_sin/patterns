<?php

abstract class Duck
{
    public $flyBehavior;
    public $quackBehavior;

    public function __construct()
    {

    }

    public abstract function display();

    public function performFly()
    {
        $this->flyBehavior->fly();
    }

    public function performQuack()
    {
        $this->quackBehavior->quack_d();
    }

    public function setFlyBehavior(FlyBehavior $fb)
    {
        $this->flyBehavior = $fb;
    }

    public function setQuackBehavior(QuackBehavior $qb)
    {
        $this->quackBehavior = $qb;
    }

    public function swim()
    {
        echo 'All ducks float, even decoy...!';
    }

}