<?php

include_once '../duck/MallarDuck.php';
include_once '../duck/ModelDuck.php';
include_once '../class/FlyRocketPowered.php';

class MiniDuckSimulator
{
    public function __construct()
    {

        $mallar = new MallarDuck();
        $mallar->performQuack();
        $mallar->performFly();

        $model = new ModelDuck();
        $model->performFly();
        $model->setFlyBehavior(new FlyRocketPowered());
        $model->performFly();

    }

}

new MiniDuckSimulator();