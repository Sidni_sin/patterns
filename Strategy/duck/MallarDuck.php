<?php

include_once '../abstract/Duck.php';
include_once '../class/Quack.php';
include_once '../class/FlyWithWings.php';

class MallarDuck extends Duck
{

    public function __construct()
    {
        $this->quackBehavior = new Quack();
        $this->flyBehavior = new FlyWithWings();

    }

    public function display()
    {
        echo 'I`m real Mallar duck<br>';
    }

}