<?php

include_once '../abstract/Duck.php';
include_once '../class/Quack.php';
include_once '../class/FlyNoWay.php';


class ModelDuck extends Duck
{

    public function __construct()
    {
        $this->flyBehavior = new FlyNoWay();
        $this->quackBehavior = new Quack();
    }

    public function display()
    {
        echo 'I`m a model duck';
    }
}