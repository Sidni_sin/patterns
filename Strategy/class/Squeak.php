<?php

include_once '../interface/QuackBehavior.php';

class Squeak implements QuackBehavior
{

    public function quack()
    {
        echo 'Squeak<br>';
    }
}