<?php

include_once '../interface/FlyBehavior.php';

class FlyRocketPowered implements FlyBehavior
{

    public function fly()
    {
        echo 'I`m flying with a rocket!';
    }

}