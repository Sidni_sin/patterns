<?php

include_once '../interface/QuackBehavior.php';

class MuteQuack implements QuackBehavior
{

    public function quack()
    {
        echo '<< Silence >><br>';
    }

}