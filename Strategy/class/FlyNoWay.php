<?php

include_once '../interface/FlyBehavior.php';

class FlyNoWay implements FlyBehavior
{

    public function fly()
    {
        echo 'I`m can`t fly<br>';
    }

}