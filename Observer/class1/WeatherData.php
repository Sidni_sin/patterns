<?php

include_once 'interface1/Subject.php';

class WeatherData implements Subject
{
    private $observers;
    private $temperature;
    private $humidity;
    private $pressure;

    public  function __construct()
    {
        $this->observers = Array();
    }

    public function registerObserver(Observer $o)
    {
        $this->observers[] = $o;
    }

    public function removeObserver(Observer $o)
    {
        $key = array_search($o, $this->observers);

        if($key >= 0){
            unset($this->observers[$key]);
        }

    }

    public function notifyObserver()
    {
        foreach ($this->observers as $observer){
            $observer->update($this->temperature, $this->humidity, $this->pressure );
        }
    }

    public function measurementsChanged()
    {
        $this->notifyObserver();
    }

    public function setMeasurements(float $temperature, float $humidity, float $pressure)
    {
        $this->temperature = $temperature;
        $this->humidity = $humidity;
        $this->pressure = $pressure;

        $this->measurementsChanged();
    }

}
